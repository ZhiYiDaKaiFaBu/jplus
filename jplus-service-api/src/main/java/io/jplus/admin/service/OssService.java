package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Oss;
import io.jplus.common.Query;

import java.util.List;

public interface OssService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Oss findById(Object id);


    /**
     * find all model
     *
     * @return all <Oss
     */
    public List<Oss> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Oss model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Oss model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Oss model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Oss model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Oss> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Oss> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Oss> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);


    public boolean deleteById(Object... ids);

    Page<Oss> queryPage(Query query);
}