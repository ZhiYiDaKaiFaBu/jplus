package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.RoleDept;

import java.util.List;

public interface RoleDeptService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public RoleDept findById(Object id);


    /**
     * find all model
     *
     * @return all <RoleDept
     */
    public List<RoleDept> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(RoleDept model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(RoleDept model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(RoleDept model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(RoleDept model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<RoleDept> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<RoleDept> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<RoleDept> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    public List<RoleDept> findListByRoleId(Object roleId);

    public boolean deleteByRoleId(Object id);

    public boolean deleteById(Object... ids);

}