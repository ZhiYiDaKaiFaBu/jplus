package io.jplus.admin.service;

import com.jfinal.plugin.activerecord.Page;
import io.jboot.db.model.Columns;
import io.jplus.admin.model.Menu;

import java.util.List;

public interface MenuService  {

    /**
     * find model by primary key
     *
     * @param id
     * @return
     */
    public Menu findById(Object id);


    /**
     * find all model
     *
     * @return all <Menu
     */
    public List<Menu> findAll();


    /**
     * delete model by primary key
     *
     * @param id
     * @return success
     */
    public boolean deleteById(Object id);


    /**
     * delete model
     *
     * @param model
     * @return
     */
    public boolean delete(Menu model);


    /**
     * save model to database
     *
     * @param model
     * @return id value if save success
     */
    public Object save(Menu model);


    /**
     * save or update model
     *
     * @param model
     * @return id value if save or update success
     */
    public Object saveOrUpdate(Menu model);


    /**
     * update data model
     *
     * @param model
     * @return
     */
    public boolean update(Menu model);


    /**
     * page query
     *
     * @param page
     * @param pageSize
     * @return page data
     */
    public Page<Menu> paginate(int page, int pageSize);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @return page data
     */
    public Page<Menu> paginateByColumns(int page, int pageSize, Columns columns);


    /**
     * page query by columns
     *
     * @param page
     * @param pageSize
     * @param columns
     * @param orderBy
     * @return page data
     */
    public Page<Menu> paginateByColumns(int page, int pageSize, Columns columns, String orderBy);

    List<Menu> findByColumns(Columns columns);

    List<Menu> findListByUserId(String userId);

    public boolean deleteById(Object... ids);

    public List<Menu> findListParentId(String pid, List<String> menuIdList);

    public List<Menu> findListParentId(String pid);

    public List<Menu> findNotButtonList();

    Menu findMenuById(Object id);
}