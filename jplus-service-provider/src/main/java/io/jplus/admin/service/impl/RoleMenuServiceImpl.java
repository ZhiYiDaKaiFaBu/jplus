package io.jplus.admin.service.impl;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.RoleMenu;
import io.jplus.admin.service.RoleMenuService;

import java.util.ArrayList;
import java.util.List;


@Bean
@RPCBean
public class RoleMenuServiceImpl extends JbootServiceBase<RoleMenu> implements RoleMenuService {

    @Override
    public List<String> queryAllMenuId(String userId) {
        SqlPara sqlPara = Db.getSqlPara("admin-rolemenu.queryAllMenuId");
        sqlPara.addPara(userId);
        List<RoleMenu> roleMenus = DAO.find(sqlPara);
        List<String> menuIdList = new ArrayList<>();
        for (RoleMenu roleMenu : roleMenus) {
            menuIdList.add(roleMenu.getMenuId());
        }
        return menuIdList;
    }

    @Override
    public List<RoleMenu> findListByRoleId(Object roleId) {
        Columns columns = Columns.create();
        columns.eq("role_id", roleId);
        List<RoleMenu> roleMenuList = DAO.findListByColumns(columns);
        return roleMenuList;
    }

    @Override
    public boolean deleteByRoleId(Object id) {
        SqlPara sqlPara = Db.getSqlPara("admin-rolemenu.deleteByRoleId");
        sqlPara.addPara(id);
        return Db.update(sqlPara) > 0 ? true : false;
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}