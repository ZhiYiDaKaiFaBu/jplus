package io.jplus.admin.service.impl;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import io.jboot.aop.annotation.Bean;
import io.jboot.components.rpc.annotation.RPCBean;
import io.jboot.db.model.Columns;
import io.jboot.service.JbootServiceBase;
import io.jplus.admin.model.Config;
import io.jplus.admin.service.ConfigService;
import io.jplus.common.Query;


@Bean
@RPCBean
public class ConfigServiceImpl extends JbootServiceBase<Config> implements ConfigService {


    @Override
    public Page<Config> queryPage(Query query) {
        Columns columns = Columns.create();
        String paramKey = (String) query.get("paramKey");
        if (StrKit.notBlank(paramKey)) {
            columns.like("param_key", paramKey);
        }
        return DAO.paginateByColumns(query.getCurrPage(), query.getLimit(), columns, query.getOrderBy());
    }

    @Override
    public Config findConfigByKey(String key) {
        Columns columns = Columns.create();
        columns.eq("param_key", key);
        return DAO.findFirstByColumns(columns);
    }

    @Override
    public boolean deleteById(Object... ids) {
        boolean tag = false;
        for (Object id : ids) {
            tag = DAO.deleteById(id);
        }
        return tag;
    }
}