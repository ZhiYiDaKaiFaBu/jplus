# Jplus后台管理系统

## 简介
这是一个基于**JBoot**实现的微服务后台管理系统脚手架。

目前实现了框架的基本搭建,及基础功能的开发，基于jboot3.0开发。

项目将持续更新，尽请期待。。。

## 模块介绍
模块名称                | 模块介绍
---                   |---
jplus-common          |公共模块，工具类、常量 
jplus-core            |核心模块
jplus-model           |模型模块
jplus-service-api     |service 接口层 
jplus-service-provider|service 实现层
jplus-web             |后台管理模块


## 项目地址
- 开源中国：https://gitee.com/retire/jplus


> 感谢jboot、renren。本后端基于jboot架构;

## 运行方法
1. 使用maven将项目导入到IDE中
2. 将docs下的sql导入数据库
5. 修改jplus-service-provider工程下的jboot.properties配置文件并启动jplus-service-provider
6. 修改jplus-web工程下的jboot.properties配置文件并启动jplus-web
7. 访问http://localhost:8080

## 注意
- Jplus需要redis与zookeeper支持

## 第三方安装（docker）

### redis
```shell script
  docker run -d -p 6379:6379 --name redis --restart always  redis
```

### zookeeper
```shell script
  docker run -d -p 2181:2181 --name zookeeper --restart always  zookeeper
```

## 打包
- mvn clean install


> 后台登录帐号:admin  
> 后台登录密码:admin


### 静态图展示
![登录](docs/images/login.png)
![后台主页](docs/images/index.png)
![列表页](docs/images/list.png)
![编辑页](docs/images/edit.png)
